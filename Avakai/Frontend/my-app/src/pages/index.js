export {default as Recipies} from './Recipies'
export { default as Home } from './Home';
export { default as Donate } from './Donate';
export { default as Profile } from './Profile';
export { default as Tiffins } from './Tiffins';
export { default as Curries } from './Curries';
export { default as Riceitems } from './Riceitems';
export { default as Snacks } from './Snacks';
export { default as Login } from './Login';
export { default as Signup } from './Signup';
export { default as Forgotpass } from './Forgotpass';